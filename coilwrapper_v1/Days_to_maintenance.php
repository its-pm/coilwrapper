<?php
$dateFormat = 'd-m-Y';

$db_con = mysqli_connect("127.0.0.1", "wp_rgk0y", "@i8J5!Dx1jkw%X^D", "wp_jpdqa");
if (!$db_con )
   die('keine Verbindung möglich: ' . mysqli_connect_error());

$db_con->set_charset("utf8");
$sql = "SELECT Maschinen.Bezeichnung, Maluspunkte.Maluspunkte, Maluspunkte.Zeitstempel, Maschinen.Verschleissgrenze
FROM Maluspunkte 
LEFT JOIN Maschinen 
	ON Maluspunkte.ID_Anlage = Maschinen.Maschinen_ID 
WHERE Maluspunkte.Wartungsintervall = Maschinen.Wartungen
ORDER BY Maluspunkte.Zeitstempel ASC;";

$ergebnis = $db_con->query($sql);

$malus_std = [];
$malus_spez = [];
$timest = [];
$timest_std = [];
$timest_spez = [];

$result_DB = array();

while($row = mysqli_fetch_row($ergebnis)) 
{
    $result_DB[] = $row;
}

#print_r(array_values($result_DB));
#echo $result_DB[0][0];
#echo count($result_DB);

for ($i=0; $i < count($result_DB); $i++)
{
	if ($result_DB[$i][0] == "Standardanlage")
	{	
		array_push($malus_std, $result_DB[$i][1]);
	    array_push($timest_std, $result_DB[$i][2]);
	    $grenze_std[] = $result_DB[$i][3];
	    $maschine_std[] = $result_DB[$i][0];
	} 
	if ($result_DB[$i][0] == "Spezialanlage")
	{
		array_push($malus_spez, $result_DB[$i][1]);
	    array_push($timest_spez, $result_DB[$i][2]);
	    $grenze_spez[] = $result_DB[$i][3];
	    $maschine_spez[] = $result_DB[$i][0];
    }
    array_push($timest, $result_DB[$i][2]);
}

#print_r(array_values($malus_std));
$machines = array($maschine_std[0], $maschine_spez[0]); 
$grenzen = array($grenze_std[0], $grenze_spez[0]); 
#print_r(array_values($machines));
// string to int

$malus_std = array_map(function($value) {
    return intval($value);
}, $malus_std);

$malus_spez = array_map(function($value) {
    return intval($value);
}, $malus_spez);

$grenzen = array_map(function($value) {
    return intval($value);
}, $grenzen);


$dates = array();
foreach ($timest AS $datetime)
{
	$old_date_timestamp = strtotime($datetime);
	$new_date = date($dateFormat, $old_date_timestamp);  
	array_push($dates, $new_date);
}
#print_r(array_values($dates));
#echo min($dates);echo("<br />\n");

$min_date = strtotime(min($dates));
$max_date = strtotime(max($dates));
$datediff = round(($max_date - $min_date ) / (60 * 60 * 24));
#echo $datediff;
if ($datediff == 0)
{
	$datediff = 1;
}

$sum_malus_std = array_sum($malus_std);
$sum_malus_spez = array_sum($malus_spez);
$sum_malus = [$sum_malus_std, $sum_malus_spez];

$mean_malus_std = $sum_malus_std/$datediff;
$mean_malus_spez = $sum_malus_spez/$datediff;
$malus_per_day = [$mean_malus_std, $mean_malus_spez];
#print_r(array_values($malus_per_day));
$lastDate = max($dates);
#echo $lastDate;echo("<br />\n");
$daysToMaint_std = round(($grenzen[0]-array_sum($malus_std))/$mean_malus_std);
$daysToMaint_spez = round(($grenzen[1]-array_sum($malus_spez))/$mean_malus_spez);
$daysToMaint = [$daysToMaint_std, $daysToMaint_spez];

$maintDate_std = date($dateFormat, strtotime($lastDate . ' + ' .$daysToMaint_std.' days'));
$maintDate_spez = date($dateFormat, strtotime($lastDate . ' + ' .$daysToMaint_spez.' days'));
$MaintDates = [$maintDate_std, $maintDate_spez];
#print_r(array_values($MaintDates));echo("<br />\n");

$h1 = 0;
foreach($machines AS $bez)
{	
	if($sum_malus[$h1] == 0)
	{
		echo "- " . $bez . ": Keine Aussage möglich, zu wenig Daten im Wartungsintervall vorhanden." ;
		echo("<br />\n");
	}
	else if ($sum_malus[$h1]<$grenzen[$h1])
	{
		echo "- " . $bez . ": " . $MaintDates[$h1] . " | " . $daysToMaint[$h1] . " Tage bis zur Wartung" ;
		echo("<br />\n");
	} 
	else 
	{
		echo "- " . $bez . ": Keine Angabe möglich --> Anlage dringend warten!";
		echo("<br />\n");
	}	
	$h1++;
}

$db_con->close();
?>