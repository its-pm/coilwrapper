<?php
// Verbindung zur Datenbank
$link = mysqli_connect("127.0.0.1", "wp_rgk0y", "@i8J5!Dx1jkw%X^D", "wp_jpdqa");
if (!$link )
   die('keine Verbindung möglich: ' . mysqli_connect_error());
$link->set_charset("utf8");

// Abfrage, welcher Kunde aktuell eingeloggt ist
$user = wp_get_current_user();

// Abfrage nach der aktuell betätigten Bestellung
$abfrage = "SELECT Bestellungen.Material_id, Bestellungen.Bestell_id, 
			Bestellungen.Kunden_id, Bestellungen.Breite_Streifen, Bestellungen.Breite_Coil, 
			Bestellungen.Anzahl_Streifen,
			Bestellungen.Coil_o_Streifen, Bestellungen.Lieferdatum,
			Bestellungen.Dicke_toleranz,Bestellungen.Name,
			Bestellungen.Lieferdatum,
			Material.Dicke, Material.Materialklasse, Material.Oberflaechenguete
			FROM Bestellungen, Material 
			where Bestellungen.Kunden_id = $user->ID and Bestellungen.Material_id=Material.Material_id 
			GROUP BY Bestell_id HAVING MAX(Bestell_id) 
			ORDER BY Bestell_id 
			desc LIMIT 1";
$ergebnis = $link->query($abfrage);

// Dartsellung der Bestellung
if ($ergebnis->num_rows > 0) 
{

	while($row = mysqli_fetch_array($ergebnis)) 
	{
		$Name = $row['Name'];
		$Bestell_id = $row['Bestell_id'];
		$Kunden_id = $row['Kunden_id'];
		$Bestelldatum = $row['Bestelldatum'];
		$Dicke = $row['Dicke'];
		$Dicketoleranz = $row['Dicke_toleranz'];
		$Was = $row['Coil_o_Streifen'];
		$Materialklasse = $row['Materialklasse'];
		$Oberflaechenguete = $row['Oberflaechenguete'];
		$Breite_Coil = $row['Breite_Coil'];
		$Breite_Streifen = $row['Breite_Streifen'];
		$Lieferdatum = $row['Lieferdatum'];
		$Anzahl_Streifen = $row['Anzahl_Streifen'];
		
		// Zur Dartstellung einer Breite (nur Coil oder Streifenbreite wird angezeigt)
		if ($Breite_Coil == 0)
		{
			$Breite_Coil = $Breite_Streifen;
		}
		
		echo "<p><h2> Bestellübersicht:</h2></p>
			<p><font size='9'><b>  Bestellnummer: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;$Bestell_id</font> </p>
			<p><font size='9'><b>  Name der Bestellung: </b> </font><font size='7'>&emsp;$Name</font> </p>
			<p><font size='9'><b>  Typ: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;$Was</font> </p>
			<p><font size='9'><b>  Dicke [mm]: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;$Dicke</font> </p>
			<p><font size='9'><b>  Dicketoleranz [mm]: </b> </font><font size='7'>&emsp;&emsp;$Dicketoleranz</font> </p>
			<p><font size='9'><b>  Materialklasse: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;&emsp;$Materialklasse</font> </p>
			<p><font size='9'><b>  Oberfächengüte: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;$Oberflaechenguete</font> </p>";
		
		// Anzahl der Streifen wird nur angezeigt, wenn diese vorhanden ist
		if ($Anzahl_Streifen > 0)
		{
			echo "<p><font size='9'><b>  Anzahl Streifen: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;&ensp;$Anzahl_Streifen</font> </p>";
		}
		echo "	
			<p><font size='9'><b>  Breite [mm]: </b> </font><font size='7'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;$Breite_Coil</font> </p>
			<p><font size='9'><b>  Vorraussichtlicher Liefertermin: </b> </font><font size='7'>&emsp;$Lieferdatum</font> </p>";	

	}

}
else
{
	echo "Keine Daten vorhanden!";
}
?>