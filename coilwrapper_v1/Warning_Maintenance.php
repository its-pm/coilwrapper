<?php
$dateFormat = 'd-m-Y';

$db_con = mysqli_connect("127.0.0.1", "wp_rgk0y", "@i8J5!Dx1jkw%X^D", "wp_jpdqa");
if (!$db_con )
   die('keine Verbindung möglich: ' . mysqli_connect_error());

$db_con->set_charset("utf8");
$sql = "SELECT Maschinen.Bezeichnung, Maluspunkte.Maluspunkte, Maluspunkte.Zeitstempel, Maschinen.Verschleissgrenze 
FROM Maluspunkte 
LEFT JOIN Maschinen 
	ON Maluspunkte.ID_Anlage = Maschinen.Maschinen_ID 
WHERE Maluspunkte.Wartungsintervall = Maschinen.Wartungen
ORDER BY Maluspunkte.Zeitstempel ASC;";
$ergebnis = $db_con->query($sql);

$malus_std = [];
$malus_spez = [];
$timest = [];
$timest_std = [];
$timest_spez = [];

$result_DB = array();

while($row = mysqli_fetch_row($ergebnis)) 
{
    $result_DB[] = $row;
}

#print_r(array_values($result_DB));
#echo $result_DB[0][0];
#echo count($result_DB);

for ($i=0; $i < count($result_DB); $i++)
{
	if ($result_DB[$i][0] == "Standardanlage")
	{	
		array_push($malus_std, $result_DB[$i][1]);
	    array_push($timest_std, $result_DB[$i][2]);
	    $grenze_std[] = $result_DB[$i][3];
	    $maschine_std[] = $result_DB[$i][0];
	} 
	if ($result_DB[$i][0] == "Spezialanlage")
	{
		array_push($malus_spez, $result_DB[$i][1]);
	    array_push($timest_spez, $result_DB[$i][2]);
	    $grenze_spez[] = $result_DB[$i][3];
	    $maschine_spez[] = $result_DB[$i][0];
    }
    array_push($timest, $result_DB[$i][2]);
}

#print_r(array_values($malus_std));
$machines = array($maschine_std[0], $maschine_spez[0]); 
$grenzen = array($grenze_std[0], $grenze_spez[0]); 
#print_r(array_values($machines));
// string to int

$malus_std = array_map(function($value) {
    return intval($value);
}, $malus_std);

$malus_spez = array_map(function($value) {
    return intval($value);
}, $malus_spez);

$grenzen = array_map(function($value) {
    return intval($value);
}, $grenzen);


$totalMalus_std = array_sum($malus_std);
$totalMalus_spez = array_sum($malus_spez);
$totalMalus = [$totalMalus_std,$totalMalus_spez];
#print_r(array_values($MaintDates));echo("<br />\n");

$h1 = 0;
$warning = false;
foreach($machines AS $bez)
{
	if ($totalMalus[$h1]>$grenzen[$h1]*0.8){
		$warning = true;
	}
	$h1++;
}

if ($warning == true){
	echo "_____________________";echo("<br />\n");
	echo "Aktuelle Warnungen:";echo("<br />\n");echo("<br />\n");

	$h1 = 0;
	$warning = false;
	foreach($machines AS $bez)
	{
		if ($totalMalus[$h1]>$grenzen[$h1]*0.8 AND $totalMalus[$h1]<=$grenzen[$h1])
		{
			echo "!!WARNUNG!! ---- ";
			echo "" . $bez . " zu ". round(($totalMalus[$h1]/$grenzen[$h1]*100)). " % verschlissen. WARTUNG VORBEREITEN!";
			echo("<br />\n");
			
		}
		if($totalMalus[$h1]>$grenzen[$h1])
		{
			echo "!!WARNUNG!! ---- ";
			echo "" . $bez . " ist verschlissen. UMGEHEND WARTUNG VORNEHMEN!";
			echo("<br />\n");
		}
		$h1++;
	}
	echo "_____________________";echo("<br />\n");echo("<br />\n");
}


$db_con->close();
?>