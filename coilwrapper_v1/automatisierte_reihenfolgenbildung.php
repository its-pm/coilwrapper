<?php

# Algorithmus zur automatisierten und nicht priorisierten Reihenfolgenbildung
#
# Autor: Patrick Walocha
# Version 5.9
# Stand: 13.01.2021
# Syntax-Kontrolle vollständig und Funktionskontrolle abgeschlossen 
# Neuer Teil: HTML-Anbindung,  Ursprungsversion: 5.4

# Bemerkung: Innerhalb des PHP-Codes sind auskommentierte Buchstaben. Diese dienen als Marker für eine mögliche Fehleranalyse.

# Präfixerklärung:
# Viele Variablen besitzen bestimmte Funktionen, die durch den Präfix ersichtlich werden.
# var: Platzhalter für Variablennamen
# tmp_var -> temporär. Diese Variable dient quasi als Zwischenspeicher
# new_var -> Neues Element, das seinen Ursprung in Var hat
# dvar -> Diese Variable dient als Sprung. Hergeleitet aus Delta
# ori_var -> Original Variable
# T, B, D, usw. 	Variablen der jeweiligen Attibute 
# arr -> Array

?>

 
 <!DOCTYPE html>
<html lang="DE">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Regeleditor</title>

</head>

<body>
<form class="user" action="" method="POST">
<p> Für die Standardfertigungsanlage: </p> 
<button type="submit" name="auto_button1" >Auto-Order</button>

<p> <label>Ab Zeile:</label>
<input type="number"  min="1" max="1000" step="1"  name="AbZeile1" placeholder="1-1000" value="1" required>
 </p>
 
 <form class="user" action="" method="POST">
<p> Für die Spezialfertigungsanlage: </p> 
<button type="submit" name="auto_button2" >Auto-Order</button>

<p> <label>Ab Zeile:</label>
<input type="number"  min="1" max="1000" step="1"  name="AbZeile2" placeholder="1-1000" value="1" required>
 </p>

<?php # Der PHP-Code

######################

# Funktion DC_Counter: Hiermit wird die Dummy-Coil-Anzahl bestimmt
# Eingabe: Lieferdatum, Attribute (Spalten), Toleranzsprung, Datenbankverbindung, Datenbanktabelle
# Ausgabe: Anzahl der Dummy-Coils

function DC_Counter($date, $column1, $column2, $delta, $connect, $actualTab) {
	
	$result = 0;
	$tmp_arr = array();

# Die jeweilige sortierte Spalte wird aus der Datenbank geholt und in einem Array gespeichert
	$sql = "SELECT $column1 FROM $actualTab WHERE Lieferdatum = '$date' ORDER BY $column2, $column1";
	$res = $connect->query($sql);	
	while($row = $res->fetch_row()){  
		array_push($tmp_arr, $row[0]);
	}
# Die Toleranzsprünge werden mittels eines Zählers bestimmt
	for($k=1;$k<count($tmp_arr);$k++){	
		$diff = $tmp_arr[$k-1] - $tmp_arr[$k];
		if(abs($diff) > $delta) {
		$result++;
		}
	}

	return $result;
	}
	
################################### Fkt-ENDE


######### Auto-Order Funktion: Hiermit wird die jeweilige Datenbank sortiert
# Eingabe: (Wunsch)Name der kopierten Datenbank, Name der ursprünglichen Datenbank, Zahl zur Ausführung des Programms, Zeilennummer, Name der Anlage
# Ausgabe: LEER

function auto_order($neueTab, $quellTab, $start, $eingabe, $TabName) {
	
# Schritt I: Daten holen und sichern
# Datenbankverbindung aufbauen
	$db_con = mysqli_connect("127.0.0.1", "wp_rgk0y", "@i8J5!Dx1jkw%X^D", "wp_jpdqa");

	if (!$db_con )
		die('ERROR! Keine Verbindung möglich!' . mysqli_connect_error());

	$db_con->set_charset("utf8");	
# Sicherheitshalber wird die Datenbank kopiert. So bleibt die ursprüngliche Datenbank erhaltne
	$newTab = $db_con->query("DROP TABLE $neueTab"); # Löschen!
	$sql_creat = "CREATE TABLE $neueTab (SELECT Coil_ID, Material_ID, Breite, Dicke, Oberflaechenguete, Materialklasse, Lieferdatum FROM $quellTab)";
	$newTab = $db_con->query($sql_creat);
	#$newTab = $db_con->query("CREATE TABLE $neueTab LIKE $quellTab"); ### Ur-Tab ändern!
	#$newDB = $db_con->query("INSERT INTO $neueTab SELECT * FROM $quellTab");
	$newDB = $newTab;
	if(!$newDB){
		echo "ERROR! CREATE nicht möglich", "<BR>";
	}


# Einfachheishalber werden globale Variablen deklariert
	$D = "Dicke";
	$B = "Breite";
	$S = "Materialklasse";
	$G = "Oberflaechenguete";
	$T = "Lieferdatum";

	$Spalte[0] = $B;
	$Spalte[1] = $D;
	$Spalte[2] = $G;
	$Spalte[3] = $S;

	$order_T_check = 0; #Kontrollvariable. Falls 1, so wird nur nach T sortiert

	$order_arr = array(); # Hier wird die Sortier-Reihenfolge abgespeichert

# Schritt II: Toleranzsprünge festlegen. Dazu wird eine Verbindung zur Datenbank des Regeleditors erstellt

# Breitensprung
	$dres = $db_con->query("SELECT Breitensprung FROM Regeleditor");
	$drow = $dres->fetch_array();
	$Deltas[0] = $drow[0];

# Dickensprung
	$dres = $db_con->query("SELECT dickensprung FROM Regeleditor");
	$drow = $dres->fetch_array();
	$Deltas[1] = $drow[0];

# Guetesprung
	$dres = $db_con->query("SELECT guetesprung FROM Regeleditor");
	$drow = $dres->fetch_array();
	$Deltas[2] = $drow[0];

# Materialklassesprung
	$dres = $db_con->query("SELECT klassensprung FROM Regeleditor");
	$drow = $dres->fetch_array();
	$Deltas[3] = $drow[0];

	if(!$Deltas[0] or !$Deltas[1] or !$Deltas[2] or !$Deltas[3]){
		echo "ERROR! Regeleditoranbindung Fehlgeschlagen";
	}

# Beginn der Reihenfolgenbildung. Durchführung erst beim Button-Klick
	if($start == 1){
		echo "Automatisierte Reihenfolgenbildung wurde durchgeführt! <BR>";

		$zeilennum = $eingabe;

# Schritt III: Erste Sortierung nach Datum. 
# Zähle die Anzahl der Zeilen ohne Wiederholungen für den Vergleich
		$sql = "SELECT COUNT(DISTINCT ".$T.") FROM $neueTab WHERE Coil_ID >= '$zeilennum'";
		$res = $db_con->query($sql);
		$row = $res->fetch_array();
		$tmp_res1 = $row[0];
# Zähle die Gesamtanzahl der Zeilen für den Vergleich
		$sql = "SELECT COUNT($T) FROM $neueTab WHERE Coil_ID >= '$zeilennum'";
		$res = $db_con->query($sql);
		$row = $res->fetch_array();
		$tmp_res2 = $row[0];
# Vergleich: Sind beide zuvor bestimmten Anzahl gleich, so reicht die Sortierung nach Datum. Denn dann wird jedes Coil an einem anderen Tag ausgeliefert
		if($tmp_res1 == $tmp_res2){
			$order_T_check = 1; 
		}
		else {
	
# Schritt IV: Sortierung der kleinen Tabelle
# Zunächst werden die Grenzen der kleineren Tabellen ermittelt
			$T_limits = $db_con->query("SELECT DISTINCT $T FROM $neueTab WHERE Coil_ID >= '$zeilennum' ORDER BY $T"); 
			$T_arr_lim = array(); 

			while($row = $T_limits->fetch_array()){  
				array_push($T_arr_lim, $row[0]);
			} 

			for($i=0; $i<count($T_arr_lim); $i++){ ####A
		
				$sql = "SELECT COUNT($T) FROM $neueTab WHERE Lieferdatum = '$T_arr_lim[$i]' AND  Coil_ID >= '$zeilennum'" ;
				$res = $db_con->query($sql);
				$tmp_row = $res->fetch_array();
				$tmp_res = $tmp_row[0];
# Nach der Festlegung der Grenzen findet die Dummy-Coil-Zählung statt 
				if($tmp_res > 1){ ####B
			
					$tmp_DC_arr = array();
					
# Schritt V: Dummy-Coil-Analyse			
# Berechnung der Dummy-Coil-Anzahl für alle Spalten mit Hilfer der DC_Counter-Funktion
					for($n=0; $n<count($Spalte); $n++){ ##### C 
						$tmp_DC_arr[$n] = DC_Counter($T_arr_lim[$i], $Spalte[$n], $Spalte[$n], $Deltas[$n], $db_con, $neueTab);
					}
			
					$tmp_min = min($tmp_DC_arr);
			
					if(count($Spalte) != count($tmp_DC_arr)){
						echo "ERROR_C: DC-CALC OR OTHER!";
					} ##### C
			
					$tmp_counter = 0; ##### D
					$tmp_wc_arr = array(); #wc = Which column; Hier werden die jeweiligen Spalten gespeichert
			
					for($p = 0; $p<count($tmp_DC_arr); $p++) {
# Berechnung der Häufigkeit der kleinsten Zahl. 
						if($tmp_DC_arr[$p] == $tmp_min){
							$tmp_wc_arr[$tmp_counter] = $p;
							$tmp_counter++;
						}
					} ##### D
# Falls die kleinste Zahl nur einmal vorhanden ist, so wird nach dessen Attribut sortiert
					if($tmp_counter == 1){ ###### E
						$order_arr[$i] = $tmp_wc_arr[0];	
					}
# Es gibt mehr als eine Spalte mit Minimum
					else if($tmp_counter > 1){ ##### F 
						
						$tmp_DC_arr2 = array(); 
# Für die bestroffenen Attribute mit der gleichen Minimumszahl wird doppelt sortiert und nochmal die Dummy-Coil-Anzahl bestimmt. 
# Bei der doppelten Sortierung werden die restlichen Atribute verwendet, dessen Dummy-Coil-Anzahl nicht der kleinsten entspricht
						for ($r=0; $r<count($tmp_wc_arr);$r++){ 
							
							for($s=0; $s<count($Spalte); $s++){ 
								if($s != $tmp_wc_arr[$r]){
									$konst_spalt = $tmp_wc_arr[$r];
									$tmp_DC_arr2[$s] = DC_Counter($T_arr_lim[$i], $Spalte[$s], $Spalte[$konst_spalt], $Deltas[$s], $db_con, $neueTab);
								}
								else {
									$tmp_DC_arr2[$s] = 9999;   # Hohe Zahl, damit die min-Funktion das richtige Ergebnis ausgibt.
								}
							}
						}
				
						$tmp_min2 = min($tmp_DC_arr2); ##### F
				
# Das Atribute mit der geringsten Zahl wird abgespeichert. Auf eine weitere Häufigkeitsbetrachtung wird einfachheitshaber Verzichtet
						$index =-1; ##### G
						for ($t=0; $t<count($tmp_DC_arr2); $t++){
							if($tmp_min2 == $tmp_DC_arr2[$t]){
								$index = $t;
							}
						}
				
						if($index > -1){
						$order_arr[$i] = $index;  
						}
						else 
							echo "ERROR_G: INDEX OR OTHER!"; ##### G 
					}
					else {
						echo "ERROR_E: COUNTER OR OTHER!";
					}	###### E
				} ####B
			else {
				$order_arr[$i] = 9;
			}
			} ####A
	
		} 
		
# Schritt VI: Engültige Sortierung und Update 

# Einführung von Bezeichnern zur Vereinfachung
		$S0='Coil_Id';
		$S1='Material_ID';
		$S2='Breite';
		$S3='Dicke';
		$S4='Oberflaechenguete';
		$S5='Materialklasse';
		$S6='Lieferdatum';
		#$S7='Breite_frei'; 
	
		$id = $zeilennum; # Zum Neusetzten der ID

		$newID = 0+$id;
# Falls alle Lieferdaten einmalig sind, so wird nur nach Datum sortiert
		if($order_T_check == 1){
				$sql_ord = "SELECT * FROM $quellTab WHERE Coil_ID >= '$zeilennum' ORDER BY Lieferdatum";
				$tmp_res = $db_con->query($sql_ord);
			
				while($row = $tmp_res->fetch_array()){  
					#$sql = "UPDATE $neueTab SET $S1 = '$row[1]', $S2 = '$row[2]', $S3 = '$row[3]', $S4 = '$row[4]', $S5 = '$row[5]', $S6 = '$row[6]', $S7 = '$row[7]' WHERE Coil_Id = '$id'"; 
					$sql = "UPDATE $neueTab SET $S0 = '$newID', $S1 = '$row[1]', $S2 = '$row[2]', $S3 = '$row[3]', $S4 = '$row[4]', $S5 = '$row[5]', $S6 = '$row[6]' WHERE Coil_Id = '$id'"; 
					
					$x = $db_con->query($sql);
					$id++;
					$newID++;
				} 
		}
# Falls mehrere Coils ein Datum teilen, so wird nach dem Dummy-Coil-Anzahl sortiert. Die Reihenfolge wurde zuvor abgespeichert. 
		else {
			
			for($a=0; $a < count($order_arr); $a++){
			
				if($order_arr[$a] == 9){
					$sql_ord = "SELECT * FROM $quellTab WHERE Lieferdatum = '$T_arr_lim[$a]' AND Coil_ID >= '$zeilennum' ORDER BY Lieferdatum";
				}
				else {
					$tmp_index = $order_arr[$a];
					$sql_ord = "SELECT * FROM $quellTab WHERE Lieferdatum = '$T_arr_lim[$a]' AND Coil_ID >= '$zeilennum' ORDER BY $Spalte[$tmp_index]";
				}
				$tmp_res = $db_con->query($sql_ord);
		
				while($row = $tmp_res->fetch_array()){  
					#$sql = "UPDATE $neueTab SET $S1 = '$row[1]', $S2 = '$row[2]', $S3 = '$row[3]', $S4 = '$row[4]', $S5 = '$row[5]', $S6 = '$row[6]', $S7 = '$row[7]' WHERE Coil_Id = '$id'";
					$sql = "UPDATE $neueTab SET $S0 = '$newID', $S1 = '$row[1]', $S2 = '$row[2]', $S3 = '$row[3]', $S4 = '$row[4]', $S5 = '$row[5]', $S6 = '$row[6]' WHERE Coil_Id = '$id'";
					
					$x = $db_con->query($sql);
					$id++;
					$newID++;
				} 
		
			}
		}

	}
	else {
		echo "Achtung: Folgende Tabelle wird ohne automatisierte Reihenfolgenbildung dargestellt! <BR>"; 
	}

 
# Ausgabe der  Tabelle
	echo "Ausgabe: ". $TabName. " <BR>";

	$tab = $db_con->query("SELECT * FROM $neueTab");
	echo "<html><table>\n";
	
	$header = $tab->fetch_fields();
	echo "<thr>\n";
	
	foreach($header as $h){
		echo "<th>".$h->name."</th>\n";
	}
	echo "</thr>";
	
	while($arr = $tab->fetch_row()){
		echo "<Tr\n>";
		
		foreach($arr as $zelle){
			echo "<td>".$zelle."</td>\n";
		}
		echo "</tr>\n";
	}

	echo "</table></html>\n";
	
}

######################################### Fkt-ENDE

$eingabe1 = $_POST['AbZeile1'];
$eingabe2 = $_POST['AbZeile2'];


if (isset($_POST['auto_button1'])){
	$start1 = 1;
}

if (isset($_POST['auto_button2'])){
	$start2 = 1;
}

$TabName1 = "Standardfertigungsanlage";
$TabName2 = "Spezialfertigungsanlage";
auto_order("MatTab", "Fertigungsplan", $start1, $eingabe1, $TabName1);
auto_order("MatTab", "Fertigungsplan_Spezialanlage", $start2, $eingabe2, $TabName2);

?>



</form>  

</body>
</html>
