<?php
$db_con = mysqli_connect("127.0.0.1", "wp_rgk0y", "@i8J5!Dx1jkw%X^D", "wp_jpdqa");
if (!$db_con )
   die('keine Verbindung möglich: ' . mysqli_connect_error());

$db_con->set_charset("utf8");
$sql = "SELECT Maschinen.Bezeichnung, Maluspunkte.Maluspunkte, Maluspunkte.Zeitstempel, Maschinen.Verschleissgrenze 
FROM Maluspunkte 
LEFT JOIN Maschinen 
	ON Maluspunkte.ID_Anlage = Maschinen.Maschinen_ID 
ORDER BY Maluspunkte.Zeitstempel ASC;";
$ergebnis = $db_con->query($sql);

$malus_std = [];
$malus_spez = [];
$timest = [];
$timest_std = [];
$timest_spez = [];

$result_DB = array();

while($row = mysqli_fetch_row($ergebnis)) 
{
    $result_DB[] = $row;
}

#print_r(array_values($result_DB));
#echo $result_DB[0][0];
#echo count($result_DB);

for ($i=0; $i < count($result_DB); $i++)
{
	if ($result_DB[$i][0] == "Standardanlage")
	{	
		array_push($malus_std, $result_DB[$i][1]);
	    array_push($timest_std, $result_DB[$i][2]);
	    $Verschleissgrenze[] = $result_DB[$i][3];
	    $maschine_std[] = $result_DB[$i][0];
	} 
	if ($result_DB[$i][0] == "Spezialanlage")
	{
		array_push($malus_spez, $result_DB[$i][1]);
	    array_push($timest_spez, $result_DB[$i][2]);
	    $Verschleissgrenze[] = $result_DB[$i][3];
	    $maschine_spez[] = $result_DB[$i][0];
    }
    array_push($timest, $result_DB[$i][2]);
}

#print_r(array_values($malus_std));
$machines = array($maschine_std[0], $maschine_spez[0]); 
#print_r(array_values($machines));
// string to int

$malus_std = array_map(function($value) {
    return intval($value);
}, $malus_std);

$malus_spez = array_map(function($value) {
    return intval($value);
}, $malus_spez);


$dates = array();
foreach ($timest AS $datetime) 
{
	$old_date_timestamp = strtotime($datetime);
	$new_date = date('Y-m-d', $old_date_timestamp);  
	array_push($dates, $new_date);
}
#echo min($dates);echo("<br />\n");

$min_date = strtotime(min($dates));
$max_date = strtotime(max($dates));
$datediff = round(($max_date - $min_date ) / (60 * 60 * 24));
#echo $datediff;

$mean_malus_std = array_sum($malus_std)/$datediff;
$mean_malus_spez = array_sum($malus_spez)/$datediff;
$malus_per_day = [$mean_malus_std, $mean_malus_spez];
#print_r(array_values($malus_per_day));

$h1 = 0;
foreach($machines AS $bez)
{
	echo "- " . $bez . ": " . round($malus_per_day[$h1]) . " Maluspunkte pro Tag";
	echo("<br />\n");
	$h1++;
}

$db_con->close();
?>